require "bundler/setup"
require "digest/sha1"
require "json"

Bundler.require

STATUSES    = ["Ignore", "Low Priority", "High Priority", "Critical"]
REPO_URL    = '' # e.g. 'https://bitbucket.org/mmmurf/monkeybadger'
REPO_BRANCH = 'master'

require File.expand_path(File.dirname(__FILE__) + "/models.rb")
require File.expand_path(File.dirname(__FILE__) + "/app.rb")
