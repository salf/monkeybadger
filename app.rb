module MonkeyBadger

  class App < Sinatra::Application

    before do
      @unclassified_count = Data.redis.scard("unclassified")
      @classified_count = Data.redis.scard("classified")
      @label_classes = {
        'Ignore' => 'success',
        'Low Priority' => 'info',
        'High Priority' => 'warning',
        'Critical' => 'danger'
      }
    end

    helpers do
      def repo_url(filepath, line_number)
        case REPO_URL
          when /github/    then REPO_URL + '/blob/' + REPO_BRANCH + filepath + "##{line_number}"
          when /bitbucket/ then REPO_URL + '/src/' + REPO_BRANCH + filepath + "#cl-#{line_number}"
          else nil
        end
      end
    end

    get '/' do
      erb :index
    end

    get '/inspect' do
      @total_words = Data.classifier.instance_variable_get(:@total_words)
      @categories = Data.classifier.instance_variable_get(:@categories)
      @max_counts = @categories.inject({}){|o,kv| o[kv[0]] = kv[1].values.max; o}
      erb :inspect
    end

    get '/reset' do
      Data.reset_classifier
      Data.reset_training
      erb :reset
    end

    get '/unclassified' do
      @unclassified = Data.redis.smembers("unclassified")[0..20]
      erb :unclassified
    end

    get '/classified' do
      @classified = Data.redis.smembers("classified")[0..20]
      @classifications = @classified.map{|sha| Data.redis.get "#{sha}_classification"}
      erb :classified
    end

    get '/train' do
      Data.train
      @count = Data.redis.scard("classified")
      erb :train
    end

    get '/classify/:sha/:classification' do |sha, classification|
      Data.classify(sha, classification)
    end

    get '/blob/:sha' do |sha|
      Data.get(sha)
    end

    get '/formated_blob/:sha' do |sha|
      @data = Data.get(sha, 'array')
      project_root_line = @data['backtrace'].select{|l| l['file']=~/\[PROJECT_ROOT\]/}.first
      if project_root_line
        @project_file_path = project_root_line['file'].gsub('[PROJECT_ROOT]', '')
        @project_line = project_root_line['number']
      end
      erb :formated_blob, layout: false
    end

    get '/create' do
      Data.create(params[:exception], params[:backtrace], params[:environment])
    end

    post '/v1/notices/' do
      exception = JSON.parse(request.body.read)
      Data.create(exception['error']['message'], exception['error']['backtrace'], exception['server']['environment_name'])
    end

  end

end
