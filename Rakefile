require File.expand_path(File.dirname(__FILE__) + "/environment.rb")
require 'net/http'
require 'uri'
require 'json'

def get_paginated_data(url_base, auth_token, limit = 100)
  results = []

  loop do
    page = (defined? response) ? response.current_page + 1 : 1

    uri = URI.parse("#{url_base}?auth_token=#{auth_token}&page=#{page}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    puts "requesting #{uri}"
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)

    if response.code == '200'
      data = JSON.parse(response.body)
      results += data['results']
      break if results.length == data['total_count'] or results.length >= limit
    else
      puts "request to #{url_base} failed with status #{response.code}"
      return nil
    end
  end

  results
end

# import errors from honeybadger for classification
task :import_from_honeybadger do
  puts "Importing error data from honeybadger for classification."
  
  url_base = 'https://api.honeybadger.io/v1'
  limit = 100

  # ask user for auth token
  puts "Please enter your API key (found on your honeybadger account page):"
  auth_token = STDIN.gets.chomp

  # load data from honeybadger api

  # load projects
  projects = get_paginated_data("#{url_base}/projects", auth_token, limit)
  projects.each do |project|
    
    # load faults by project
    faults = get_paginated_data("#{url_base}/projects/#{project['id']}/faults", auth_token, limit)
    faults.each do |fault|

      # load notices by fault
      notices = get_paginated_data("#{url_base}/projects/#{project['id']}/faults/#{fault['id']}/notices", auth_token, limit)
      notices.each do |notice|
        MonkeyBadger::Data.create "#{fault['klass']}: #{fault['message']}", notice['cgi_data'], fault['environment']
      end

    end

  end

  puts "import complete"

end

task :import_from_file do
  puts "Importing error data from file for classification"

  File.open('data.json', 'r') do |file| 
    data = JSON.parse(file.read).each do |notice|
      MonkeyBadger::Data.create *notice
    end

    puts "imported #{data.length} items"
  end

end