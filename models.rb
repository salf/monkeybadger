module MonkeyBadger

  class Helpers
    def self.sha(blob)
      Digest::SHA1.hexdigest blob
    end
  end

  class Data
    @@classifier = nil

    def self.reset_classifier
      @@classifier = Classifier::Bayes.new(*STATUSES)
      Data.redis.set("classifier", nil)
    end

    def self.reset_training
      Data.redis.sadd("classified", "unclassified")
      Data.redis.sdiffstore "classified", "classified", "classified"
    end

    def self.redis
      @@redis ||= Redis.new
    end

    def self.train
      Data.reset_classifier
      Data.redis.smembers("classified").each do |sha|
        classification = Data.redis.get("#{sha}_classification")
        blob = Data.redis.get(sha)
        Data.classifier.send("train_#{classification}", blob)
      end
      marshalled = Marshal.dump(classifier)
      packed = [marshalled].pack('m')
      Data.redis.set("classifier", packed)
    end

    def self.classify(sha, classification)
      Data.redis.set("#{sha}_classification", classification)
      Data.redis.smove("unclassified", "classified", sha)
    end

    def self.get(sha, array = false)
      if array
        %w(exception backtrace environment).inject({}) do |res, key|
          data = Data.redis.get("#{sha}_#{key}")
          res[key] = JSON::parse(data) rescue data
          res
        end
      else
        Data.redis.get(sha)
      end
    end

    def self.classifier
      if @@classifier.nil?
        packed = redis.get("classifier")
        unpacked = packed.unpack('m')[0] rescue nil
        if packed && unpacked && packed.size > 2
          @@classifier ||= Marshal.load(unpacked)
        else
          @@classifier = Classifier::Bayes.new(*STATUSES)
        end
      else
        @@classifier
      end
    end

    def self.create(exception, backtrace, environment)

      # concat them for classification but strip out the slashes
      blob = "#{exception} #{backtrace} #{environment}"
      stripped_blob = blob.gsub(/\//, ' ')

      sha = Helpers.sha(stripped_blob)
      redis.incr("count_#{sha}")

      if redis.sismember("classified", sha)
        redis.get("#{sha}_classification")
      else
        redis.sadd("unclassified", sha)
        redis.set(sha, blob)

        redis.set("#{sha}_exception", exception.to_json)
        redis.set("#{sha}_backtrace", backtrace.to_json)
        redis.set("#{sha}_environment", environment.to_json)

        result = {:result => classifier.classify(stripped_blob)}
        result.merge(classifier.classifications(stripped_blob)).inspect # append the classifier scores to output for testing
      end
    end

  end
end
